/*
 * MGSwipeTableCell is licensed under MIT license. See LICENSE.md file for more information.
 * Copyright (c) 2014 Imanol Fernandez @MortimerGoro
 */

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MGSwipeTableCell.h"

@interface ListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate, UIActionSheetDelegate,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, assign) BOOL testingStoryboardCell;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *selectedArray;

@end
