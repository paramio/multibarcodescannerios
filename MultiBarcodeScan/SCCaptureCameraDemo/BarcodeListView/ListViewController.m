/*
 * MGSwipeTableCell is licensed under MIT license. See LICENSE.md file for more information.
 * Copyright (c) 2014 Imanol Fernandez @MortimerGoro
 */

#import "ListViewController.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "SCCaptureCameraController.h"
#import "SCCommon.h"
#define TEST_USE_MG_DELEGATE 1

@implementation ListViewController
{
    NSMutableDictionary * dictionaryData;
    NSMutableArray * keyArray;
    UIBarButtonItem * prevButton;
    UIBarButtonItem * leftPrevButton;
    UITableViewCellAccessoryType accessory;
    UIImageView * background; //used for transparency test
    BOOL allowMultipleSwipe;
    NSString *alertMsg;
    NSString *SENDEALL;
    NSString *DELETEALL;
    UIRefreshControl * refreshControl;
    NSString *CANCEL;
    NSString *OK;
    NSString *_NO;
}


-(void) cancelTableEditClick: (id) sender
{
    [_tableView setEditing: NO animated: YES];
    self.navigationItem.rightBarButtonItem = prevButton;
    self.navigationItem.leftBarButtonItem = leftPrevButton;
    prevButton = nil;
    leftPrevButton = nil;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    if (buttonIndex == 1) {
        _tableView.allowsMultipleSelectionDuringEditing = YES;
        [_tableView setEditing: YES animated: YES];
        prevButton = self.navigationItem.rightBarButtonItem;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:CANCEL style:UIBarButtonItemStyleDone target:self action:@selector(cancelTableEditClick:)];
        leftPrevButton = self.navigationItem.leftBarButtonItem;        
        self.navigationItem.leftBarButtonItem =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(multiDelete)];
        alertMsg = DELETEALL;
        [self alertButtonClicked];
    }
    else if(buttonIndex == 2){
        NSLog(@"send email");
        //[self sendEMail];
        _tableView.allowsMultipleSelectionDuringEditing = YES;
        [_tableView setEditing: YES animated: YES];
        prevButton = self.navigationItem.rightBarButtonItem;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:CANCEL style:UIBarButtonItemStyleDone target:self action:@selector(cancelTableEditClick:)];
        leftPrevButton = self.navigationItem.leftBarButtonItem;
        UIButton *scanButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
        [scanButton setBackgroundImage:[UIImage imageNamed:@"email.png"] forState:UIControlStateNormal];
        [scanButton addTarget:self action:@selector(sendEMail) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:scanButton];
        alertMsg = SENDEALL;
        [self alertButtonClicked];
    }else{}
}

-(void) actionClick: (id) sender
{
    
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"sheet_title",@"") delegate:self cancelButtonTitle:CANCEL destructiveButtonTitle:nil otherButtonTitles: nil];
    [sheet addButtonWithTitle: NSLocalizedString(@"delete",@"")];
    [sheet addButtonWithTitle: NSLocalizedString(@"send",@"")];
    [sheet showInView:self.view];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"ButtonIndex,%ld", (long)buttonIndex);
    
    if(buttonIndex == 1){
        _selectedArray = (NSMutableArray *)[_tableView indexPathsForVisibleRows];
        if(alertMsg == DELETEALL)
        {
            NSLog(@"deleteAlert");
            [self multiDelete];
        }else if(alertMsg == SENDEALL)
        {
            NSLog(@"sendAlert");
            [self sendEMail];
        }else{}
        [self cancelTableEditClick:nil];
    }
}
-(void) alertButtonClicked{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:alertMsg
                                                   delegate:self
                                          cancelButtonTitle:_NO
                                          otherButtonTitles:OK,nil];
    [alert show];
}

//点击按钮后，触发这个方法
-(void)sendEMail
{
    int count = (int)[self.selectedArray count];
    if (count > 0 ) {
        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
        if (mailClass != nil)
        {
            if ([mailClass canSendMail])
            {
                [self displayComposerSheet];
            }
            else
            {
                [self launchMailAppOnDevice];
            }
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"caution",@"") message:NSLocalizedString(@"selected_none",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"reselect",@"") otherButtonTitles:nil];
        [alert show];

    }
}
//可以发送邮件的话
-(void)displayComposerSheet
{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    
    mailPicker.mailComposeDelegate = self;
    
    //设置主题
    //[mailPicker setSubject: @"Barcodes"];
    
    // 添加发送者
    //NSArray *toRecipients = [NSArray arrayWithObject: @"first@example.com"];
    //NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil];
    //NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com", nil];
    //[mailPicker setToRecipients: toRecipients];
    //[picker setCcRecipients:ccRecipients];
    //[picker setBccRecipients:bccRecipients];
    
    // 添加图片
    //UIImage *addPic = [UIImage imageNamed: @"123.jpg"];
    //NSData *imageData = UIImagePNGRepresentation(addPic);            // png
    // NSData *imageData = UIImageJPEGRepresentation(addPic, 1);    // jpeg
    //[mailPicker addAttachmentData: imageData mimeType: @"" fileName: @"123.jpg"];
    NSString *emailBody = @"IMEI,ICCID,UPC,PN,SN \t\n";

    int count = (int)_selectedArray.count;
    for (int i=0; i < count; i++) {
        int row = (int)[[_selectedArray objectAtIndex:i]row];
        NSString *keyStr = [keyArray objectAtIndex:row];
        NSString *data = [dictionaryData objectForKey:keyStr];
        int index = (int)[data rangeOfString:@")"].location;
        emailBody = [emailBody stringByAppendingString:[data substringToIndex:index+1]];
         emailBody = [emailBody stringByAppendingString:@": "];
        emailBody = [emailBody stringByAppendingString:keyStr];
        emailBody = [emailBody stringByAppendingString:[data substringFromIndex:index+1]];
        emailBody = [emailBody stringByAppendingString:@"\n"];
        keyStr = nil;
    }
    [mailPicker setMessageBody:emailBody isHTML:YES];
    emailBody = nil;
    [_selectedArray removeAllObjects];
    [self presentViewController: mailPicker animated:YES completion:nil];
}
-(void)launchMailAppOnDevice
{
    NSString *recipients = @"mailto:first@example.com&subject=my email!";
    //@"mailto:first@example.com?cc=second@example.com,third@example.com&subject=my email!";
    NSString *body = @"&body=email body!";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:email]];
}
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *msg;
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            msg = NSLocalizedString(@"email_canceled",@"");
            [self alertWithTitle:nil msg:msg];
            break;
        case MFMailComposeResultSaved:
            msg = NSLocalizedString(@"email_saved",@"");
            [self alertWithTitle:nil msg:msg];
            break;
        case MFMailComposeResultSent:
            msg = NSLocalizedString(@"email_sended",@"");
            [self alertWithTitle:nil msg:msg];
            break;
        case MFMailComposeResultFailed:
            msg = NSLocalizedString(@"email_failed",@"");
            [self alertWithTitle:nil msg:msg];
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) alertWithTitle: (NSString *)_title_ msg: (NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:_title_
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:OK
                                          otherButtonTitles:nil];
    [alert show];
}
-(void) refreshCallback
{
    [_tableView reloadData];
    [refreshControl endRefreshing];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"app_title",@"");
    _selectedArray = [[NSMutableArray alloc]initWithCapacity:1];
    SENDEALL = NSLocalizedString(@"send_all",@"");
    DELETEALL = NSLocalizedString(@"delete_all",@"");
    CANCEL = NSLocalizedString(@"cancel",@"");
    OK = NSLocalizedString(@"ok",@"");
    _NO = NSLocalizedString(@"no",@"");
    
    if (!_testingStoryboardCell) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [self.view addSubview:_tableView];
        refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(refreshCallback) forControlEvents:UIControlEventValueChanged];
        [self.tableView addSubview:refreshControl];
    }
    
    self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionClick:)];
    // 左边 按钮 自定义图片
    UIButton *scanButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [scanButton setBackgroundImage:[UIImage imageNamed:@"scanbarcode.png"] forState:UIControlStateNormal];
    [scanButton addTarget:self action:@selector(startCaptrueScan) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithCustomView:scanButton];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    //self.navigationItem.leftBarButtonItem =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:nil];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 当 从扫描界面  返回时 调用一边 reload 重新加载数据
    [_tableView reloadData];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
- (void)startCaptrueScan{
    SCCaptureCameraController *captureController = [[SCCaptureCameraController alloc]init];
    // 设置效果
    [captureController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];    
    // 底部向上  UIModalTransitionStyleCoverVertical
    // 淡入     UIModalTransitionStyleCrossDissolve
    // 翻转     UIModalTransitionStyleFlipHorizontal
    // 翻半页   UIModalTransitionStylePartialCurl
    // 跳转
    [self presentViewController:captureController animated:YES completion:nil];
}

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray * result = [NSMutableArray array];
    MGSwipeButton * button = [MGSwipeButton buttonWithTitle:NSLocalizedString(@"delete",@"") backgroundColor:[UIColor redColor] callback:^BOOL(MGSwipeTableCell * sender){
            NSLog(@"Convenience callback received (right).");
            BOOL autoHide = NO;
            return autoHide; //Don't autohide in delete button to improve delete expansion animation
        }];
    [result addObject:button];
    return result;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    // FOR RELOAD
    // 读取指定文件
    dictionaryData = [SCCommon readDataFromFile:@"multibarcodescan.txt"];
    // return all keys
    keyArray = (NSMutableArray *)[dictionaryData objectForKey:@"indexArray"];
    // resever all objects
    keyArray = (NSMutableArray *)[[keyArray reverseObjectEnumerator] allObjects];
    // show barcode rows
    self.title = NSLocalizedString(@"app_title",@"");
    self.title = [self.title stringByAppendingString:@"("];
    self.title = [self.title stringByAppendingString:[NSString stringWithFormat:@"%lu",(unsigned long)keyArray.count]];
    self.title = [self.title stringByAppendingString:@")"];
    return keyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGSwipeTableCell * cell;
    
 
    /**
    *  using programmatically created cells
    **/
    static NSString * reuseIdentifier = @"programmaticCell";
    cell = [_tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    }
    
    
    //TestData *dao data = [tests objectAtIndex:indexPath.row];
    //int ii = (int)indexPath.row;
    NSString *keyStr = [keyArray objectAtIndex:indexPath.row];
    NSString *detailText = [dictionaryData objectForKey:keyStr];
    
    cell.textLabel.text = [@"IMEI:" stringByAppendingString:keyStr];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.detailTextLabel.text = detailText;
    // 自动 换行
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
    detailText = nil;
    keyStr = nil;
    cell.accessoryType = accessory;
    cell.delegate = self;
    cell.allowsMultipleSwipe = allowMultipleSwipe;
    
    if (background) { //transparency test
        cell.backgroundColor = [UIColor clearColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
        cell.selectedBackgroundView.backgroundColor = [[UIColor yellowColor] colorWithAlphaComponent:0.3];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.swipeBackgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor yellowColor];
        cell.detailTextLabel.textColor = [UIColor yellowColor];
    }
    return cell;
}

#if TEST_USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    // MGSwipeTransitionBorder,
    // MGSwipeTransitionStatic,
    // MGSwipeTransitionClipCenter,
    // MGSwipeTransitionDrag,
    // MGSwipeTransition3D
    swipeSettings.transition = MGSwipeTransitionBorder;
    
    if (direction == MGSwipeDirectionLeftToRight) {
        expansionSettings.buttonIndex = -1;
        expansionSettings.fillOnTrigger = NO;
        return nil;
    }
    else {
        expansionSettings.buttonIndex = -1;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:1];
    }
}
#endif


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        NSIndexPath * path = [_tableView indexPathForCell:cell];
        // remove obeject from dictionary
        [dictionaryData removeObjectForKey:[keyArray objectAtIndex:path.row]];
        // must remove the data in array ,or it will error
        [keyArray removeObjectAtIndex:path.row];
        // resever all objects
        NSMutableArray *nKeyArray = (NSMutableArray *)[[keyArray reverseObjectEnumerator] allObjects];
        [dictionaryData setObject:nKeyArray forKey:@"indexArray"];
        // rewrite dictionaryData to file
        [SCCommon writeDataToFile:dictionaryData fileName:@"multibarcodescan.txt"];
        [_tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
        return NO; //Don't autohide to improve delete expansion animation
    }
    
    return YES;
}
- (void)multiDelete{
    int count = (int)[self.selectedArray count];
    if (count > 0 ) {
        for (int i=0; i<count; i++) {
            int row = (int)[[_selectedArray objectAtIndex:i]row];
            // remove obeject from dictionary
            [dictionaryData removeObjectForKey:[keyArray objectAtIndex:row]];
            // must remove the data in array ,or it will error
            [keyArray setObject:@"deleted" atIndexedSubscript:row];
        }
        // delete the item has been set "deleted"
        [keyArray removeObject:@"deleted"];
        // resever all objects
        NSMutableArray *nKeyArray = (NSMutableArray *)[[keyArray reverseObjectEnumerator] allObjects];
        [dictionaryData setObject:nKeyArray forKey:@"indexArray"];
        // rewrite dictionaryData to file
        [SCCommon writeDataToFile:dictionaryData fileName:@"multibarcodescan.txt"];
        
        [_tableView deleteRowsAtIndexPaths:self.selectedArray withRowAnimation:UITableViewRowAnimationFade];
        
        [self.selectedArray removeAllObjects];

        self.navigationItem.leftBarButtonItem = leftPrevButton;
        self.navigationItem.rightBarButtonItem = prevButton;
        [_tableView setEditing:NO animated:YES];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"caution",@"") message:NSLocalizedString(@"selected_none",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"reselect",@"") otherButtonTitles:nil];
        [alert show];
        //[alert release];
    }  
}

#pragma tableView delegate methods
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}
//添加一项
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.navigationItem.rightBarButtonItem.title isEqualToString:CANCEL]) {
        [self.selectedArray addObject:indexPath];
    }
}

//取消一项
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.navigationItem.rightBarButtonItem.title isEqualToString:CANCEL]) {
        [self.selectedArray removeObject:indexPath];
        //        NSLog(@"Deselect---->:%@",self.selectedArray);
    }
}

//选择后
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    //
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped accessory button");
}

@end
