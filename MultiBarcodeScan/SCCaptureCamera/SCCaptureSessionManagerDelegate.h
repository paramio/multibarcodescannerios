
@class SCCaptureSessionManager;
@class ZXCGImageLuminanceSource;
@protocol SCCaptureSessionManagerDelegate <NSObject>

- (void)captureResult:(SCCaptureSessionManager *)capture resultArray:(NSMutableArray *)resultArray resultSource:(ZXCGImageLuminanceSource *) source;

@optional
- (void)captureSize:(SCCaptureSessionManager *)capture
              width:(NSNumber *)width
             height:(NSNumber *)height;

- (void)captureCameraIsReady:(SCCaptureSessionManager *)capture;

@end
