
#import <UIKit/UIKit.h>
#import "SCCaptureSessionManager.h"
#import "SCCaptureSessionManagerDelegate.h"

@interface SCCaptureCameraController : UIViewController<SCCaptureSessionManagerDelegate>

@property (nonatomic, assign) CGRect previewRect;
@property (nonatomic, assign) BOOL isStatusBarHiddenBeforeShowCamera;


@end
