//
//  IphoneModelsData.h
//  MultiBarcodeScan
//
//  Created by mio on 15-3-18.
//  Copyright (c) 2015年 CumuliTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IphoneModelsData : NSObject
@property(nonatomic, readonly) NSMutableDictionary *modelDatas;
-(NSString *)getOneModel:(NSString *) partNo;
-(id)init;
@end
