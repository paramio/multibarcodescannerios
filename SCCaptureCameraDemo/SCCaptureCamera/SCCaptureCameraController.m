//
//  SCCaptureCameraController.m
//  SCCaptureCameraDemo
//
//  Created by Aevitx on 14-1-16.
//  Copyright (c) 2014年 Aevitx. All rights reserved.
//

#import "SCCaptureCameraController.h"
#import "SCSlider.h"
#import "SCCommon.h"
#import "SVProgressHUD.h"
#import "ZXCGImageLuminanceSource.h"
#import "SCNavigationController.h"
#import "ZXResult.h"

//static void * CapturingStillImageContext = &CapturingStillImageContext;
//static void * RecordingContext = &RecordingContext;
//static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

#define SWITCH_SHOW_FOCUSVIEW_UNTIL_FOCUS_DONE      0   //对焦框是否一直闪到对焦完成

#define SWITCH_SHOW_DEFAULT_IMAGE_FOR_NONE_CAMERA   1   //没有拍照功能的设备，是否给一张默认图片体验一下

//height
#define CAMERA_TOPVIEW_HEIGHT   44  //title
#define CAMERA_MENU_VIEW_HEIGH  44  //menu

//color
#define bottomContainerView_UP_COLOR     [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.f]       //bottomContainerView的上半部分
#define bottomContainerView_DOWN_COLOR   [UIColor colorWithRed:68/255.0f green:68/255.0f blue:68/255.0f alpha:1.f]       //bottomContainerView的下半部分
#define DARK_GREEN_COLOR        [UIColor colorWithRed:10/255.0f green:107/255.0f blue:42/255.0f alpha:1.f]    //深绿色
#define LIGHT_GREEN_COLOR       [UIColor colorWithRed:143/255.0f green:191/255.0f blue:62/255.0f alpha:1.f]    //浅绿色


//对焦
#define ADJUSTINT_FOCUS @"adjustingFocus"
#define LOW_ALPHA   0.7f
#define HIGH_ALPHA  1.0f

//typedef enum {
//    bottomContainerViewTypeCamera    =   0,  //拍照页面
//    bottomContainerViewTypeAudio     =   1   //录音页面
//} BottomContainerViewType;

@interface SCCaptureCameraController () {
    int alphaTimes;
    CGPoint currTouchPoint;
}

@property (nonatomic, strong) SCCaptureSessionManager *captureManager;

@property (nonatomic, strong) UIView *topContainerView;//顶部view
@property (nonatomic, strong) UILabel *topLbl;//顶部的标题

@property (nonatomic, strong) UIView *bottomContainerView;//除了顶部标题、拍照区域剩下的所有区域
@property (nonatomic, strong) UIView *cameraMenuView;//网格、闪光灯、前后摄像头等按钮
@property (nonatomic, strong) NSMutableSet *cameraBtnSet;

@property (nonatomic, strong) UIView *doneCameraUpView;
@property (nonatomic, strong) UIView *doneCameraDownView;

//对焦
@property (nonatomic, strong) UIImageView *focusImageView;

@property (nonatomic, strong) SCSlider *scSlider;
//计时器
@property (nonatomic,strong) NSTimer *showTimer;

//@property (nonatomic) id runtimeErrorHandlingObserver;
//@property (nonatomic) BOOlockInterfaceRotation;
// 扫描 框
@property (nonatomic, strong) UIView *scanView;
@end

@implementation SCCaptureCameraController

#pragma mark -------------life cycle---------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        alphaTimes = -1;
        currTouchPoint = CGPointZero;
        
        _cameraBtnSet = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //navigation bar
    if (self.navigationController && !self.navigationController.navigationBarHidden) {
        self.navigationController.navigationBarHidden = YES;
    }
    
    //status bar
    if (!self.navigationController) {
        _isStatusBarHiddenBeforeShowCamera = [UIApplication sharedApplication].statusBarHidden;
        if ([UIApplication sharedApplication].statusBarHidden == NO) {
            //iOS7，需要plist里设置 View controller-based status bar appearance 为NO
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        }
    }
    
    //notification
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationOrientationChange object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange:) name:kNotificationOrientationChange object:nil];
    
    //session manager
    SCCaptureSessionManager *manager = [[SCCaptureSessionManager alloc] init];
    
    //AvcaptureManager
    if (CGRectEqualToRect(_previewRect, CGRectZero)) {
        //self.previewRect = CGRectMake(0, 0, SC_APP_SIZE.width, SC_APP_SIZE.width + CAMERA_TOPVIEW_HEIGHT);
        self.previewRect = [self view].frame;
    }
    [manager configureWithParentLayer:self.view previewRect:_previewRect];
    self.captureManager = manager;
    
    //[self addTopViewWithText:@"拍照"];
    //[self addbottomContainerView];
    //[self addPinchGesture];
    [self addFocusView];
    [self addCameraCover];
    [self addScanCover];
    
    
    
#if SWITCH_SHOW_DEFAULT_IMAGE_FOR_NONE_CAMERA
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [SVProgressHUD showErrorWithStatus:@"设备不支持拍照功能!"];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        NSLog(@"%f",self.view.frame.size.width);
        imgView.clipsToBounds = YES;
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"nocamera" ofType:@"jpg"]];
        [self.view addSubview:imgView];
        [self.view sendSubviewToBack:imgView];
    }
#endif
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CGFloat centerX = _scanView.frame.origin.x;
    CGFloat centerY = _scanView.frame.origin.y;
    CGFloat centerWidth = _scanView.frame.size.width;
    CGFloat centerHeight = _scanView.frame.size.height;
    // 扫描线
    [SCCommon addScanLine:_scanView width:centerHeight-40 color:[UIColor whiteColor].CGColor speed:0.2];
    
    // 扫描区域 设置
    // 1920 1080
    CGAffineTransform captureSizeTransform = CGAffineTransformMakeScale(1280 / self.view.frame.size.height, 720 / self.view.frame.size.width);
//    NSLog(@"SELFVIEW:width=%f,height=%f",self.view.frame.size.width,self.view.frame.size.height);
    // revesal x y
    CGRect reversalScanView = CGRectMake(centerY, centerX, centerHeight, centerWidth);
    CGRect scanScale = CGRectApplyAffineTransform(reversalScanView, captureSizeTransform);
//    NSLog(@"origin rect x:%f,y:%f,w:%f,h:%f",_scanView.frame.origin.x,_scanView.frame.origin.y,_scanView.frame.size.width,_scanView.frame.size.height);
    //NSLog(@"scale rect x:%f,y:%f,w:%f,h:%f",scanScale.origin.x,scanScale.origin.y,scanScale.size.width,scanScale.size.height);
    _captureManager.scanRect = scanScale;
    _captureManager.delegate = self;
    // 启动 拍摄
    [_captureManager.session startRunning];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self addCameraMenuView];
}
//- (void)viewWillAppear:(BOOL)animated {
//	dispatch_async(_captureManager.sessionQueue, ^{
//		[self addObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
//		[self addObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
//		[self addObserver:self forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];
//		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[_captureManager.inputDevice device]];
//
//		WEAKSELF_SC
//		[self setRuntimeErrorHandlingObserver:[[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification object:_captureManager.session queue:nil usingBlock:^(NSNotification *note) {
//			SCCaptureCameraController *strongSelf = weakSelf_SC;
//			dispatch_async(strongSelf.captureManager.sessionQueue, ^{
//				// Manually restarting the session since it must have been stopped due to an error.
//				[strongSelf.captureManager.session startRunning];
//			});
//		}]];
//		[_captureManager.session startRunning];
//	});
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//	dispatch_async(_captureManager.sessionQueue, ^{
//		[_captureManager.session stopRunning];
//
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[_captureManager.inputDevice device]];
//		[[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
//
//		[self removeObserver:self forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
//		[self removeObserver:self forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
//		[self removeObserver:self forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
//	});
//}
- (NSString *)barcodeFormatToString:(ZXBarcodeFormat)format {
    switch (format) {
        case kBarcodeFormatAztec:
            return @"Aztec";
            
        case kBarcodeFormatCodabar:
            return @"CODABAR";
            
        case kBarcodeFormatCode39:
            return @"Code 39";
            
        case kBarcodeFormatCode93:
            return @"Code 93";
            
        case kBarcodeFormatCode128:
            return @"Code 128";
            
        case kBarcodeFormatDataMatrix:
            return @"Data Matrix";
            
        case kBarcodeFormatEan8:
            return @"EAN-8";
            
        case kBarcodeFormatEan13:
            return @"EAN-13";
            
        case kBarcodeFormatITF:
            return @"ITF";
            
        case kBarcodeFormatPDF417:
            return @"PDF417";
            
        case kBarcodeFormatQRCode:
            return @"QR Code";
            
        case kBarcodeFormatRSS14:
            return @"RSS 14";
            
        case kBarcodeFormatRSSExpanded:
            return @"RSS Expanded";
            
        case kBarcodeFormatUPCA:
            return @"UPCA";
            
        case kBarcodeFormatUPCE:
            return @"UPCE";
            
        case kBarcodeFormatUPCEANExtension:
            return @"UPC/EAN extension";
            
        default:
            return @"Unknown";
    }
}

- (void)captureResult:(SCCaptureSessionManager *)capture resultArray:(NSMutableArray *)resultArray resultSource:(ZXCGImageLuminanceSource *) source{
    if (resultArray.count != 5) return;
    [_captureManager.session stopRunning];
    NSLog(@"IAMHERE!");
    [self addCameraCover];
    [self showCameraCover:YES];
    [self showResults:resultArray resultSource:source];
    [_captureManager.session stopRunning];
    [_captureManager.resultArray removeAllObjects];
    
    
}
- (void)showResults:(NSMutableArray *)resultArray resultSource:(ZXCGImageLuminanceSource *) source{
    //初始化
    CGFloat resultWidth = SC_APP_SIZE.width;
    CGFloat width = _scanView.frame.size.height/2;
    CGFloat height = _scanView.frame.size.width/2;
    CGFloat onePiece = 15;
    CGFloat stepHeight = height + onePiece * 2;

    UIView *resultsView = [[UIView alloc]initWithFrame:CGRectMake(0, SC_APP_SIZE.height*2/5, resultWidth, resultWidth/2)];
    resultsView.transform = CGAffineTransformMakeRotation(M_PI_2);//旋转90度
   
    UIImageView  *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(5, 0, width, height)];
    CGImageRef image = source.image;
    UIImage *uiImage = [UIImage imageWithCGImage: image];
    [imageView setImage:uiImage];
    [resultsView addSubview:imageView];
    uiImage = nil;
    imageView = nil;    
    
    for (int i=0; i<resultArray.count; i++) {
        ZXResult * result = (ZXResult *)[resultArray objectAtIndex:i];
        UILabel *resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, stepHeight, resultWidth, onePiece)];
        NSString * text = [[self barcodeFormatToString:result.barcodeFormat] stringByAppendingString:@": "];
        text = [text stringByAppendingString: result.text];
        resultLabel.textColor = [UIColor whiteColor];
        resultLabel.font = [UIFont fontWithName:@"Arial"  size:15];
        resultLabel.text = text;
        [resultsView addSubview:resultLabel];
        text = nil;
        resultLabel = nil;
        result = nil;
        stepHeight = stepHeight + onePiece;
    }
    [_doneCameraUpView addSubview:resultsView];
    resultsView = nil;
    // timer
    _showTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timeOver) userInfo:nil repeats:NO];
    
}
- (void)timeOver{
    [_captureManager.session startRunning];
    [self showCameraCover:NO];
    [_showTimer invalidate];
    _showTimer = nil;
    self.doneCameraUpView = nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    if (!self.navigationController) {
        if ([UIApplication sharedApplication].statusBarHidden != _isStatusBarHiddenBeforeShowCamera) {
            [[UIApplication sharedApplication] setStatusBarHidden:_isStatusBarHiddenBeforeShowCamera withAnimation:UIStatusBarAnimationSlide];
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationOrientationChange object:nil];
    
#if SWITCH_SHOW_FOCUSVIEW_UNTIL_FOCUS_DONE
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device && [device isFocusPointOfInterestSupported]) {
        [device removeObserver:self forKeyPath:ADJUSTINT_FOCUS context:nil];
    }
#endif
    
    self.captureManager = nil;
    self.doneCameraUpView =nil;
}

#pragma mark -------------UI---------------
//扫描层
- (void)addScanCover{
    // 总容器
    CGRect frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    UIView *scanView = [[UIView alloc] initWithFrame:frame];
    [self.view addSubview:scanView];
    CGFloat width = scanView.bounds.size.width;
    CGFloat height = scanView.bounds.size.height;
    // 左侧
    CGRect leftFrame = CGRectMake(0, 0, width/4, height);
    UIView *leftScanView = [[UIView alloc] initWithFrame:leftFrame];
    leftScanView.backgroundColor = [UIColor blackColor];
    leftScanView.alpha = 0.4;
    [scanView addSubview:leftScanView];
    // 右侧
    CGRect rightFrame = CGRectMake(width/4*3, 0, width/4, height);
    UIView *rightScanView = [[UIView alloc] initWithFrame:rightFrame];
    rightScanView.backgroundColor = [UIColor blackColor];
    rightScanView.alpha = 0.4;
    [scanView addSubview:rightScanView];
    // 上侧
    CGRect topFrame = CGRectMake(width/4, 0, width/2, height/10);
    UIView *topScanView = [[UIView alloc] initWithFrame:topFrame];
    topScanView.backgroundColor = [UIColor blackColor];
    topScanView.alpha = 0.4;
    [scanView addSubview:topScanView];
    // 下侧
    CGRect bottomFrame = CGRectMake(width/4, height/10*9, width/2, height/10);
    UIView *bottomScanView = [[UIView alloc] initWithFrame:bottomFrame];
    bottomScanView.backgroundColor = [UIColor blackColor];
    bottomScanView.alpha = 0.4;
    [scanView addSubview:bottomScanView];
    // 中间扫描区域
    CGRect centerFrame = CGRectMake(width/4, height/10, width/2, height/5*4);
    UIView *centerScanView = [[UIView alloc] initWithFrame:centerFrame];
    [scanView addSubview:centerScanView];
    // for crop scan image
    self.scanView = centerScanView;
    // 画 四个角
    //CGRectMake(eachW, 0, 1, CAMERA_MENU_VIEW_HEIGH) andColor:rgba_SC(102, 102, 102, 1.0000) inLayer:_cameraMenuView.layer
    CGFloat centerWidth = centerScanView.frame.size.width;
    CGFloat centerHeight = centerScanView.frame.size.height;
    // 左上角
    [SCCommon drawALineWithFrame:CGRectMake(0, 0, 20, 5)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    [SCCommon drawALineWithFrame:CGRectMake(0, 0, 5, 20)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    // 右上角
    [SCCommon drawALineWithFrame:CGRectMake(centerWidth-20, 0, 20, 5)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    [SCCommon drawALineWithFrame:CGRectMake(centerWidth-5, 0, 5, 20)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    // 左下角
    [SCCommon drawALineWithFrame:CGRectMake(0, centerHeight-5, 20, 5)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    [SCCommon drawALineWithFrame:CGRectMake(0, centerHeight-20, 5, 20)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    // 右下角
    [SCCommon drawALineWithFrame:CGRectMake(centerWidth-20, centerHeight-5, 20, 5)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    [SCCommon drawALineWithFrame:CGRectMake(centerWidth-5, centerHeight-20, 5, 20)  andColor:rgba_SC(255, 255, 255, 1.0000) inLayer:centerScanView.layer];
    
    
}
//顶部标题
- (void)addTopViewWithText:(NSString*)text {
    if (!_topContainerView) {
        CGRect topFrame = CGRectMake(0, 0, SC_APP_SIZE.width, CAMERA_TOPVIEW_HEIGHT);
        
        UIView *tView = [[UIView alloc] initWithFrame:topFrame];
        tView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:tView];
        self.topContainerView = tView;
        
        UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, topFrame.size.width, topFrame.size.height)];
        emptyView.backgroundColor = [UIColor blackColor];
        emptyView.alpha = 0.4f;
        [_topContainerView addSubview:emptyView];
        
        topFrame.origin.x += 10;
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, topFrame.size.height)];
        lbl.backgroundColor = [UIColor clearColor];
        lbl.textColor = [UIColor whiteColor];
        lbl.font = [UIFont systemFontOfSize:25.f];
        [_topContainerView addSubview:lbl];
        self.topLbl = lbl;
    }
    _topLbl.text = text;
}

//bottomContainerView，总体
- (void)addbottomContainerView {
    
    CGFloat bottomY = _captureManager.previewLayer.frame.origin.y + _captureManager.previewLayer.frame.size.height;
    CGRect bottomFrame = CGRectMake(0, bottomY, SC_APP_SIZE.width, SC_APP_SIZE.height - bottomY);
    
    UIView *view = [[UIView alloc] initWithFrame:bottomFrame];
    view.backgroundColor = bottomContainerView_UP_COLOR;
    [self.view addSubview:view];
    self.bottomContainerView = view;
}

//拍照菜单栏
- (void)addCameraMenuView {
    
    //拍照按钮
//    CGFloat downH = (isHigherThaniPhone4_SC ? CAMERA_MENU_VIEW_HEIGH : 0);
//    CGFloat cameraBtnLength = 90;
//    [self buildButton:CGRectMake((SC_APP_SIZE.width - cameraBtnLength) / 2, (_bottomContainerView.frame.size.height - downH - cameraBtnLength) / 2 , cameraBtnLength, cameraBtnLength)
//         normalImgStr:@"shot.png"
//      highlightImgStr:@"shot_h.png"
//       selectedImgStr:@""
//               action:@selector(takePictureBtnPressed:)
//           parentView:_bottomContainerView];
    
    
    //拍照的菜单栏view（屏幕高度大于480的，此view在上面，其他情况在下面）
    CGFloat menuViewX = (isHigherThaniPhone4_SC ? SC_DEVICE_SIZE.width - CAMERA_MENU_VIEW_HEIGH : 0);
    UIView *menuView = [[UIView alloc] initWithFrame:CGRectMake(menuViewX, 0, CAMERA_MENU_VIEW_HEIGH, self.view.frame.size.height/3)];
//    menuView.backgroundColor = (isHigherThaniPhone4_SC ? bottomContainerView_DOWN_COLOR : [UIColor clearColor]);
    [self.view addSubview:menuView];
    [self.view bringSubviewToFront:menuView];
    self.cameraMenuView = menuView;
    
    [self addMenuViewButtons];
}

//拍照菜单栏上的按钮
- (void)addMenuViewButtons {
    NSMutableArray *normalArr = [[NSMutableArray alloc] initWithObjects:@"logout.png", @"share.png", @"setting.png", nil];
    NSMutableArray *highlightArr = [[NSMutableArray alloc] initWithObjects:@"logout_h.png", @"share_h.png", @"setting_h.png", nil];
//    NSMutableArray *selectedArr = [[NSMutableArray alloc] initWithObjects:@"", @"camera_line_h.png", @"switch_camera_h.png", @"", nil];
    
    NSMutableArray *actionArr = [[NSMutableArray alloc] initWithObjects:@"logoutBtnPressed:", @"shareBtnPressed:", @"settingBtnPressed:", nil];
    // 单个 按钮的 高度
    CGFloat eachH = SC_APP_SIZE.height / (3 * actionArr.count);
    
//    [SCCommon drawALineWithFrame:CGRectMake(eachW, 0, 1, CAMERA_MENU_VIEW_HEIGH) andColor:rgba_SC(102, 102, 102, 1.0000) inLayer:_cameraMenuView.layer];
    
    
    //屏幕高度大于480的，后退按钮放在_cameraMenuView；小于480的，放在_bottomContainerView
    for (int i = 0; i < actionArr.count; i++) {
        
//        CGFloat theH = (!isHigherThaniPhone4_SC && i == 0 ? _bottomContainerView.frame.size.height : CAMERA_MENU_VIEW_HEIGH);
//        UIView *parent = (!isHigherThaniPhone4_SC && i == 0 ? _bottomContainerView : _cameraMenuView);
        CGFloat theH = CAMERA_MENU_VIEW_HEIGH;
        UIView *parent = _cameraMenuView;
        UIButton * btn = [self buildButton:CGRectMake(0, eachH * i, theH, eachH)
                              normalImgStr:[normalArr objectAtIndex:i]
                           highlightImgStr:[highlightArr objectAtIndex:i]
                            selectedImgStr:nil
                                    action:NSSelectorFromString([actionArr objectAtIndex:i])
                                parentView:parent];
        
        btn.showsTouchWhenHighlighted = YES;
        
        [_cameraBtnSet addObject:btn];
    }
}

- (UIButton*)buildButton:(CGRect)frame
            normalImgStr:(NSString*)normalImgStr
         highlightImgStr:(NSString*)highlightImgStr
          selectedImgStr:(NSString*)selectedImgStr
                  action:(SEL)action
              parentView:(UIView*)parentView {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    if (normalImgStr.length > 0) {
        [btn setImage:[UIImage imageNamed:normalImgStr] forState:UIControlStateNormal];
    }
    if (highlightImgStr.length > 0) {
        [btn setImage:[UIImage imageNamed:highlightImgStr] forState:UIControlStateHighlighted];
    }
//    if (selectedImgStr.length > 0) {
//        [btn setImage:[UIImage imageNamed:selectedImgStr] forState:UIControlStateSelected];
//    }
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [parentView addSubview:btn];
    
    return btn;
}

//对焦的框
- (void)addFocusView {
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"touch_focus_x.png"]];
    imgView.alpha = 0;
    [self.view addSubview:imgView];
    self.focusImageView = imgView;
    
#if SWITCH_SHOW_FOCUSVIEW_UNTIL_FOCUS_DONE
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device && [device isFocusPointOfInterestSupported]) {
        [device addObserver:self forKeyPath:ADJUSTINT_FOCUS options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    }
#endif
}

//拍完照后的遮罩
- (void)addCameraCover {
    UIView *upView = [[UIView alloc] initWithFrame:CGRectMake(SC_APP_SIZE.width, 0, 0, SC_APP_SIZE.height)];
    upView.backgroundColor = [UIColor blackColor];
    upView.alpha = 0.9;
    [self.view addSubview:upView];
    self.doneCameraUpView = upView;
    
//    UIView *downView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, SC_APP_SIZE.height)];
//    downView.backgroundColor = [UIColor blackColor];
//    [self.view addSubview:downView];
//    self.doneCameraDownView = downView;
}

- (void)showCameraCover:(BOOL)toShow {
    
    [UIView animateWithDuration:0.38f animations:^{
        CGRect upFrame = _doneCameraUpView.frame;
        upFrame.origin.x = (toShow ? 0 : SC_APP_SIZE.width);
        upFrame.size.width = (toShow ? SC_APP_SIZE.width: 0);
        _doneCameraUpView.frame = upFrame;
        
//        CGRect downFrame = _doneCameraDownView.frame;
//
//        downFrame.size.width = (toShow ? SC_APP_SIZE.width : 0);
//        _doneCameraDownView.frame = downFrame;
    }];
}

//伸缩镜头的手势
- (void)addPinchGesture {
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self.view addGestureRecognizer:pinch];
    
    //横向
    //    CGFloat width = _previewRect.size.width - 100;
    //    CGFloat height = 40;
    //    SCSlider *slider = [[SCSlider alloc] initWithFrame:CGRectMake((SC_APP_SIZE.width - width) / 2, SC_APP_SIZE.width + CAMERA_MENU_VIEW_HEIGH - height, width, height)];
    
    //竖向
    CGFloat width = 40;
    CGFloat height = _previewRect.size.height - 100;
    SCSlider *slider = [[SCSlider alloc] initWithFrame:CGRectMake(_previewRect.size.width - width, (_previewRect.size.height + CAMERA_MENU_VIEW_HEIGH - height) / 2, width, height) direction:SCSliderDirectionVertical];
    slider.alpha = 0.f;
    slider.minValue = MIN_PINCH_SCALE_NUM;
    slider.maxValue = MAX_PINCH_SCALE_NUM;
    
    WEAKSELF_SC
    [slider buildDidChangeValueBlock:^(CGFloat value) {
        [weakSelf_SC.captureManager pinchCameraViewWithScalNum:value];
    }];
    [slider buildTouchEndBlock:^(CGFloat value, BOOL isTouchEnd) {
        [weakSelf_SC setSliderAlpha:isTouchEnd];
    }];
    
    [self.view addSubview:slider];
    
    self.scSlider = slider;
}

void c_slideAlpha() {
    
}

- (void)setSliderAlpha:(BOOL)isTouchEnd {
    if (_scSlider) {
        _scSlider.isSliding = !isTouchEnd;
        
        if (_scSlider.alpha != 0.f && !_scSlider.isSliding) {
            double delayInSeconds = 3.88;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                if (_scSlider.alpha != 0.f && !_scSlider.isSliding) {
                    [UIView animateWithDuration:0.3f animations:^{
                        _scSlider.alpha = 0.f;
                    }];
                }
            });
        }
    }
}

#pragma mark -------------touch to focus---------------
#if SWITCH_SHOW_FOCUSVIEW_UNTIL_FOCUS_DONE
//监听对焦是否完成了
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:ADJUSTINT_FOCUS]) {
        BOOL isAdjustingFocus = [[change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1] ];
        //        SCDLog(@"Is adjusting focus? %@", isAdjustingFocus ? @"YES" : @"NO" );
        //        SCDLog(@"Change dictionary: %@", change);
        if (!isAdjustingFocus) {
            alphaTimes = -1;
        }
    }
}

- (void)showFocusInPoint:(CGPoint)touchPoint {
    
    [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        
        int alphaNum = (alphaTimes % 2 == 0 ? HIGH_ALPHA : LOW_ALPHA);
        self.focusImageView.alpha = alphaNum;
        alphaTimes++;
        
    } completion:^(BOOL finished) {
        
        if (alphaTimes != -1) {
            [self showFocusInPoint:currTouchPoint];
        } else {
            self.focusImageView.alpha = 0.0f;
        }
    }];
}
#endif

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //    [super touchesBegan:touches withEvent:event];
    
    alphaTimes = -1;
    
    UITouch *touch = [touches anyObject];
    currTouchPoint = [touch locationInView:self.view];
    
    if (CGRectContainsPoint(_captureManager.previewLayer.bounds, currTouchPoint) == NO) {
        return;
    }
    
    [_captureManager focusInPoint:currTouchPoint];
    
    //对焦框
    [_focusImageView setCenter:currTouchPoint];
    _focusImageView.transform = CGAffineTransformMakeScale(2.0, 2.0);
    
#if SWITCH_SHOW_FOCUSVIEW_UNTIL_FOCUS_DONE
    [UIView animateWithDuration:0.1f animations:^{
        _focusImageView.alpha = HIGH_ALPHA;
        _focusImageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
        [self showFocusInPoint:currTouchPoint];
    }];
#else
    [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _focusImageView.alpha = 1.f;
        _focusImageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5f delay:0.5f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            _focusImageView.alpha = 0.f;
        } completion:nil];
    }];
#endif
}

#pragma mark -------------button actions---------------
//拍照页面，拍照按钮
- (void)takePictureBtnPressed:(UIButton*)sender {
#if SWITCH_SHOW_DEFAULT_IMAGE_FOR_NONE_CAMERA
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [SVProgressHUD showErrorWithStatus:@"设备不支持拍照功能T_T"];
        return;
    }
#endif
    
    sender.userInteractionEnabled = NO;
    
    [self showCameraCover:YES];
    
    __block UIActivityIndicatorView *actiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actiView.center = CGPointMake(self.view.center.x, self.view.center.y - CAMERA_TOPVIEW_HEIGHT);
    [actiView startAnimating];
    [self.view addSubview:actiView];
    
    WEAKSELF_SC
    [_captureManager takePicture:^(UIImage *stillImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [SCCommon saveImageToPhotoAlbum:stillImage];//存至本机
        });
        
        [actiView stopAnimating];
        [actiView removeFromSuperview];
        actiView = nil;
        
        double delayInSeconds = 2.f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            sender.userInteractionEnabled = YES;
            [weakSelf_SC showCameraCover:NO];
        });
        
        //your code 0
        SCNavigationController *nav = (SCNavigationController*)weakSelf_SC.navigationController;
        if ([nav.scNaigationDelegate respondsToSelector:@selector(didTakePicture:image:)]) {
            [nav.scNaigationDelegate didTakePicture:nav image:stillImage];
        }
        //or your code 1
//        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationTakePicture object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:stillImage, kImage, nil]];
        
        //or your code 2
//    PostViewController *con = [[PostViewController alloc] init];
//    con.postImage = stillImage;
//    [self.navigationController pushViewController:con animated:YES];
    }];
}

- (void)tmpBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// 退出 程序 按钮
- (void)logoutBtnPressed:(id)sender {
//    if (self.navigationController) {
//        if (self.navigationController.viewControllers.count == 1) {
//            [self.navigationController dismissModalViewControllerAnimated:YES];
//        } else {
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//    } else {
//        [self dismissModalViewControllerAnimated:YES];
//    }
    [_captureManager logOutApp];
}


// 分享按钮
- (void)shareBtnPressed:(UIButton*)sender {
//    sender.selected = !sender.selected;
//    [_captureManager switchGrid:sender.selected];
    [_captureManager shareImages];
}

// 设置按钮
- (void)settingBtnPressed:(UIButton*)sender {
//    sender.selected = !sender.selected;
//    [_captureManager switchCamera:sender.selected];
    [_captureManager settingOption];
}

//拍照页面，闪光灯按钮
- (void)flashBtnPressed:(UIButton*)sender {
//    [_captureManager switchFlashMode:sender];
}

#pragma mark -------------pinch camera---------------
//伸缩镜头
- (void)handlePinch:(UIPinchGestureRecognizer*)gesture {
    
    [_captureManager pinchCameraView:gesture];
    
    if (_scSlider) {
        if (_scSlider.alpha != 1.f) {
            [UIView animateWithDuration:0.3f animations:^{
                _scSlider.alpha = 1.f;
            }];
        }
        [_scSlider setValue:_captureManager.scaleNum shouldCallBack:NO];
        
        if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
            [self setSliderAlpha:YES];
        } else {
            [self setSliderAlpha:NO];
        }
    }
}


#pragma mark ------------notification-------------
- (void)orientationDidChange:(NSNotification*)noti {
    
    //    [_captureManager.previewLayer.connection setVideoOrientation:(AVCaptureVideoOrientation)[UIDevice currentDevice].orientation];
    
    if (!_cameraBtnSet || _cameraBtnSet.count <= 0) {
        return;
    }
    [_cameraBtnSet enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        UIButton *btn = ([obj isKindOfClass:[UIButton class]] ? (UIButton*)obj : nil);
        if (!btn) {
            *stop = YES;
            return ;
        }
        
        btn.layer.anchorPoint = CGPointMake(0.5, 0.5);
        CGAffineTransform transform = CGAffineTransformMakeRotation(0);
        switch ([UIDevice currentDevice].orientation) {
            case UIDeviceOrientationPortrait://1
            {
                transform = CGAffineTransformMakeRotation(0);
                break;
            }
            case UIDeviceOrientationPortraitUpsideDown://2
            {
                transform = CGAffineTransformMakeRotation(M_PI);
                break;
            }
            case UIDeviceOrientationLandscapeLeft://3
            {
                transform = CGAffineTransformMakeRotation(M_PI_2);
                break;
            }
            case UIDeviceOrientationLandscapeRight://4
            {
                transform = CGAffineTransformMakeRotation(-M_PI_2);
                break;
            }
            default:
                break;
        }
        [UIView animateWithDuration:0.3f animations:^{
            btn.transform = transform;
        }];
    }];
}

#pragma mark ---------rotate(only when this controller is presented, the code below effect)-------------
//<iOS6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOrientationChange object:nil];
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_6_0
//iOS6+
- (BOOL)shouldAutorotate
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOrientationChange object:nil];
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    //    return [UIApplication sharedApplication].statusBarOrientation;
	return UIInterfaceOrientationPortrait;
}
#endif

@end
